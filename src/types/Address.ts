
//Type Address : street (texte), city (texte), postalCode (texte), country (texte).
export type Address = {
    street: string
    city: string
    postalCode: string
    country: string
}