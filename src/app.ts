import { Clothing } from "./classes/héritage-classes/Clothing"
import { ClothingSize } from "./classes/héritage-classes/Clothing"
import { Customer } from "./classes/Customer"
import { Product } from "./classes/Product"

// Nouvelle Instance de la classe Customer 
const Paul = new Customer(1, 'Paul', 'paul@gmail.com')
console.log(Paul)
Paul.setAddress({ street: '47 avenue de Paris', city: 'Toulouse', postalCode: '31320', country: 'France' })
console.log(Paul.displayInfo())
console.log(Paul.displayAddress())


const productInstance = new Product(2, 't-shirt blanc', 225, 1, { lenght: 1, widht: 2, height: 4 })
console.log(productInstance)
console.log(productInstance.displayDetails())

const short = new Clothing(3,'short en lin', 12, 58, {lenght : 30, widht : 90, height : 10}, ClothingSize.M)
console.log(short)