import { Product } from "../../classes/Product"
import { Dimensions } from "../../types/Dimensions"

export enum ClothingSize {
    XS = 'XS',
    S = 'S',
    M = 'M',
    L = 'L',
    XL = 'XL',
    XXL = 'XXL'

}

export enum ShoeSize {
    SIZE36 = 36,
    SIZE37,
    SIZE38,
    SIZE39,
    SIZE40,
    SIZE41,
    SIZE42,
    SIZE43,
    SIZE44,
    SIZE45,
    SIZE46

}
export class Clothing extends Product {
    size: ClothingSize;

    constructor(productId: number, name: string, weight: number, price: number, dimension: Dimensions, size: ClothingSize) {
        super(productId, name, weight, price, dimension)
        this.size = size
    }
    displayDetails(): string {
        //J'utilise super.displayDetails() pour appeler la méthode displayDetails() de la classe parente (Product).
        //J'ajoute ensuite les propriétés spécifiques de la sous-classe
        return `${super.displayDetails()},  Size : ${this.size}`
    }

}

export class Shoes extends Product {
    size: ShoeSize;

    constructor(productId: number, name: string, weight: number, price: number, dimension: Dimensions, size: ShoeSize) {
        super(productId, name, weight, price, dimension)
        this.size = size
    }
    displayDetails(): string {

        return super.displayDetails() + "Hello"

    }
}

