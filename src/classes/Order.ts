import { Customer } from "./Customer"
import { Product } from "./Product"
class Order {
    orderId: number
    customer: Customer
    productList: Product[]
    orderDate: Date

    constructor(
        orderId: number,
        customer: Customer,
        productList: Product[],
        orderDate: Date
    ) {
       
        this.orderId = orderId
        this.customer = customer
        this.productList = productList
        this.orderDate = orderDate
    }
}