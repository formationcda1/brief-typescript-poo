import { Dimensions } from "../types/Dimensions"
/*Classe Product :
Propriétés :
productId (numérique)
name (texte)
weight (numérique)
price (numérique)
Méthodes :
displayDetails(): string : Retourne les détails d'un produit au format "Product ID: $productId, Name: $name, Weight: $weight, Price: $price €"
*/

// gérer un inventaire de produits (Product)
export class Product {
    productId: number
    name: string
    weight: number
    price: number
    dimensions: Dimensions

    constructor(productId: number, name: string, weight: number, price: number, dimension: Dimensions) {
        this.productId = productId
        this.name = name
        this.weight = weight
        this.price = price
        this.dimensions = dimension
    }
    //Retourne les détails d'un produit (type : string)
    displayDetails(): string {
        return `Product ID : ${this.productId}, Name : ${this.name}, Weight : ${this.weight}, Price : ${this.price}, Lenght : ${this.dimensions.lenght}, Width : ${this.dimensions.widht}, Height: ${this.dimensions.height}`
    }
}