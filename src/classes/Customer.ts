import { Address } from "../types/Address"

//Ce système devra permettre de créer et de gérer des clients (Customer)
/* Class Customer = 
Propriétés :
customerId (numérique)
name (texte)
email (texte)
Méthodes :
displayInfo(): string: Retourne les informations d'un client au format "Customer ID: $customerId, Name: $name, Email: $email"
*/

//gérer des clients (Customer)
export class Customer {
    // Encapsulation public: est accessible par tout le monde (valeur par défautt si on ne précise rien)
    public customerId: number
    public name: string
    public email: string
    public address: Address | undefined
    //constructeur(hydratation de la classe) il initialise les valeurs des propriétés (Par défaut si constructeur non renseigné la classe possède un constructeur vide qui ne renseigne aucune propriété)
    constructor(customerId: number, name: string, email: string, /*address : Address*/) {
        //this représente l'nstance de la classe en court
        this.customerId = customerId
        this.name = name
        this.email = email
        /*  this.address = address*/
    }
    // Méthode displayInfo(de type string) retourne les information du clients
    displayInfo(): string {
        return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, ${this.displayAddress()}`
    }
    // la méthode setAddress renseigne l'addresse du client
    setAddress(address: Address) {
        this.address = address
    }
    displayAddress(): string {
        if (this.address != undefined) {
            return ` Address : Street : ${this.address.street}, City : ${this.address.city}, Postal Code : ${this.address.postalCode}, Country : ${this.address.country}`
        } else {
            return " No address found"
        }
    }
}
